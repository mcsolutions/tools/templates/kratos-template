GOPRIVATE:=gitlab.com
GROUP:=$project$
REGISTRY:=$registry$
REPOSITORY:=$repo$
DOCKER_IMAGE_NAME:=$(REGISTRY).$(REPOSITORY)
CONFIG_VOLUME:=$configVolume$
PORT_HTTP:=$portHttp$
PORT_GRPC:=$portGrpc$
MUSEUM:=$museum$
CHART:=$chart$
RELEASE_NAME:=$backNameVersion$
STAGING:=$staging$

GOPATH:=$(shell go env GOPATH)
VERSION:=$(shell git describe --tags --always)
GIT_COMMIT:=$(shell git rev-list -1 HEAD)
CONF_PROTO_FILES:=$(shell find internal/conf -name *.proto)

print:
	@echo "GOPATH" $(GOPATH)
	@echo "VERSION" $(VERSION)
	@echo "GIT_COMMIT" $(GIT_COMMIT)
	@echo "CONF_PROTO_FILES" $(CONF_PROTO_FILES)

version:
	@echo $(VERSION)

v: version

clone-configs-local:
	@mkdir -p ../../configs
	@git clone https://$projectGroup$/configs/local.git ../../configs/local

remove-api-third_party:
	@rm -f -r \
	api \
	third_party \
	internal/biz/README.md \
	internal/data/README.md \
	internal/service/README.md \
	openapi.yaml \
	generate.go

change-internal:
	@sed -i 's|greeter|$service$|g' internal/*/*.go
	@sed -i 's|Greeter|$Service$|g' internal/*/*.go
	@mv internal/biz/greeter.go internal/biz/$name$.go
	@mv internal/data/greeter.go internal/data/$name$.go
	@mv internal/service/greeter.go internal/service/$name$.go
	@sed -i 's|v1\.|pb\.|g' internal/*/*.go
	@sed -i 's|v1 "$repo$/api/helloworld/v1"|pb "$projectGroup$/proto/gen/go/api/$servicePackage$"|g' internal/*/*.go
	@goimports -w internal/server internal/service
	@wire gen ./cmd/$nameVersion$

change-mod:
	@sed -i "s|go 1.16|go 1.17|g" go.mod
	@echo "\nreplace $projectGroup$/proto => ../../proto" >> go.mod
	@echo "\nreplace $projectGroup$/back/common => ../common" >> go.mod
	@go mod download
	@go mod tidy

change-main:
	@sed -i 's|Name string|Name = \"$nameVersion$\"|g' cmd/$nameVersion$/main.go
	@sed -i 's|flagconf|flagConf|g' cmd/$nameVersion$/main.go
	@sed -i 's|bc|conf|g' cmd/$nameVersion$/main.go
	@sed -i 's|../../configs|./configs|g' cmd/$nameVersion$/main.go
	@goimports -w cmd

copy-config:
	@rm -rf configs-local
	@cp    ../../configs/local/back/$nameVersion$/.env .
	@cp -r ../../configs/local/back/$nameVersion$/docker-compose.yaml .
	@cp -a ../../configs/local/back/$nameVersion$/configs configs-local

change-config:
	@sed -i 's|8000|$(PORT_HTTP)|g' configs/config.yaml
	@sed -i 's|9000|$(PORT_GRPC)|g' configs/config.yaml
	@sed -i 's|0.0.0.0:|:|g' configs/config.yaml

init-wire:
	@go mod vendor
	@go get github.com/google/wire/cmd/wire@v0.5.0
	@wire gen ./cmd/$nameVersion$

init-all: remove-api-third_party change-internal change-mod change-main change-config
	@rm -f -r .git
	@go mod vendor
	@du -shc vendor/*
	@git init --initial-branch=master
	@git remote add origin https://$repo$.git
	@git add .
	@git commit -q -F tag_message
	@git tag -a v0.0.1 -m Init

init-ent:
	@ent init --target ./internal/data/ent/schema $Service$
	@go get entgo.io/ent
	@go mod vendor
	@ent generate ./internal/data/ent/schema
	@echo "package ent" > ./internal/data/ent/generate.go
	@echo "\n//go:generate go run -mod=mod entgo.io/ent/cmd/ent generate ./schema" >> ./internal/data/ent/generate.go

.PHONY: config
config:
	@protoc \
		--proto_path=. \
		--proto_path=$(GOPATH)/src/$projectGroup$/proto/third_party \
 		--go_out=paths=source_relative:. \
		$(CONF_PROTO_FILES)

.PHONY: dep
dep:
	@go mod download
	@go mod vendor
	@go mod tidy -compat=1.17

.PHONY: wire
wire:
	@wire gen ./cmd/$nameVersion$

.PHONY: ent
ent:
	@ent generate ./internal/data/ent/schema

.PHONY: build
build:
	@mkdir -p bin/ && \
	GO111MODULE=on \
	go build -mod=vendor \
	-ldflags "-s -w -X main.Version=$(VERSION)" \
	-o ./bin/ ./...

.PHONY: compose
compose:
	@docker-compose up -d

.PHONY: compose-stop
compose-stop:
	@docker-compose stop

run:
	GO111MODULE=on go run -ldflags "-s -w -X main.Version=$(VERSION)" ./cmd/$nameVersion$/

test:
	GO111MODULE=on go test -v -race ./...

push:
	@git status
	@git push origin master
	@git push --tags

build-container:
	@docker build -t $(DOCKER_IMAGE_NAME):$(VERSION) .

run-container:
	@docker run -d \
	-p $(PORT_HTTP):$(PORT_HTTP) \
	-p $(PORT_GRPC):$(PORT_GRPC) \
	-v $(GOPATH)/src/$projectGroup$/configs:$(CONFIG_VOLUME) \
	--name=$nameVersion$ \
	$(DOCKER_IMAGE_NAME):$(VERSION)

push-container:
	docker tag $(DOCKER_IMAGE_NAME):$(VERSION) $(DOCKER_IMAGE_NAME):latest
	docker push $(DOCKER_IMAGE_NAME):$(VERSION)
	docker push $(DOCKER_IMAGE_NAME):latest

rmi:
	docker rmi $(docker images | grep $(DOCKER_IMAGE_NAME))

images:
	docker images $(DOCKER_IMAGE_NAME)

port-forward:
	kubectl -n $(STAGING) port-forward $(kubectl get pods -n $(STAGING) -l app=$(RELEASE_NAME) -o name) 1$(PORT):$(PORT)

template:
	helm repo add $(GROUP) $(MUSEUM)
	helm repo update
	helm template $(RELEASE_NAME) $(GROUP)/$(CHART) -f=values.yaml > template.yaml

install:
	helm install $(RELEASE_NAME) $(GROUP)/$(CHART) -n $(STAGING) -f=values.yaml

uninstall:
	helm uninstall $(RELEASE_NAME) -n $(STAGING)

.PHONY: create-service
create-service:
	mkdir -p internal/service
	find api -name "*.proto" -exec kratos proto server {} \;

.PHONY: tags
tags:
	@git tag --sort=-creatordate -n | head -n 10